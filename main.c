#include <SDL2/SDL_video.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

#include <SDL2/SDL.h>

const char * title      = "title.";
const uint32_t width    = 800;
const uint32_t height   = 600;

void render(SDL_Surface * surface, uint32_t x, uint32_t y, uint32_t * p) {
    *p = SDL_MapRGBA(surface->format, x, y, 0, 255);
}

int main ( void ) {

    SDL_Window *    window;
    SDL_Surface *   surface;

    SDL_Event       events;



    if(SDL_Init( SDL_INIT_EVERYTHING )) return EXIT_FAILURE;

    window = SDL_CreateWindow(title, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, width, height, SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE );


    if(!window) return EXIT_FAILURE;

    surface = SDL_GetWindowSurface(window);


    bool quit = false;

    do {

    uint64_t start = SDL_GetTicks64();

    while (SDL_PollEvent(&events))
        switch (events.type) {
            case SDL_QUIT:
                quit = true;
                break;

            default:
                break;
        }

    //SDL_memset(surface->pixels, SDL_MapRGBA(surface->format, 88, 11, 88, 255), surface->h * surface->pitch);

    SDL_LockSurface(surface);

    for (uint32_t y = 0; y < surface->h; y++) 
        for(uint32_t x = 0; x < surface->w; x++) {
            uint32_t * it = (uint32_t*)((uint8_t*)surface->pixels + y*surface->pitch + x*surface->format->BytesPerPixel);
            //*it = SDL_MapRGBA(surface->format, x, y, 0, 255);
            render(surface, x, y, it);
        }

    SDL_UnlockSurface(surface);


    SDL_UpdateWindowSurface(window);

    uint64_t work = SDL_GetTicks64() - start;

    if(16 > work) SDL_Delay(16 - (SDL_GetTicks64() - start));
    
    
    } while(!quit);


    SDL_DestroyWindow(window);

    SDL_Quit();


    return EXIT_SUCCESS;
}