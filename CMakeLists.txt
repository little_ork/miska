cmake_minimum_required(VERSION 3.22.0)
project(miska VERSION 0.1.0)


set(CMAKE_INTERPROCEDURAL_OPTIMIZATION_RELEASE OFF)

# did it really works?

##[===[


if( NOT X11_FOUND ) 
    set( SDL_X11 OFF CACHE BOOL "" FORCE )
    set( SDL_WAYLAND_LIBDECOR_SHARED OFF CACHE BOOL "" FORCE )
    
    set( PKG_LIBDECOR_FOUND TRUE CACHE BOOL "" FORCE )

    set( SDL_WAYLAND_LIBDECOR ON CACHE BOOL "" FORCE )
    set( SDL_HIDAPI ON CACHE BOOL "" FORCE )
endif()

set( SDL_TEST OFF CACHE BOOL "" FORCE )
set( SDL_STATIC ON CACHE BOOL "" FORCE )
set( SDL_SHARED OFF CACHE BOOL "" FORCE )

add_subdirectory(external/SDL/)

#]===]

#find_package(SDL2 REQUIRED)

file(GLOB_RECURSE __src 
    "src/*.h"
    "src/*.c"
)

include_directories(miska ${SDL2_INCLUDE_DIRS})

add_executable(miska main.c ${__src})

target_link_libraries(miska PRIVATE SDL2-static SDL2::SDL2main )

# it just doesnt work. There is a conflict with gtk theme. Cairo is the ugliest theme ever been, so the there are only two ways of fixing the problem
# * Use Xwayland and X11 protocols in sdl
# * Make own wayland bindings =/

# I lose 16 hours to recognise that D: